package edu.umgc.cmis141.teasdale.methods;

	import java.util.Scanner;

	public class Methods
	{
		public static void main(String[] args)
		{
			//initialize variables and scanner object

			int continueUh = 0;
			
			Scanner in = new Scanner(System.in);

			//Creating main loop to continue until the user wishes to exit

			while(continueUh != 9)
			{
				//Prompt user with menus

				displayMenu();
				

				//Passing scanner object to the validation method and afterwards assigning the result to the continueUh variable
				continueUh = inputValidation(in);
				
				//Using a switch case to have user's choice from menu prompt an action
				switch(continueUh)
				{
					//Converting cubic feet to bushels by taking cubic feet input and passing it to the conversion method
					case 1:
					    System.out.println("");
						System.out.println("Enter cubic feet:");
						double cubicFeet = in.nextDouble();
						System.out.println("");
						System.out.printf("%.2f cubic ft. = %.5f U.S. bushels\n", cubicFeet, cubicFeetToUSBushels(cubicFeet));
						System.out.println("");
						break;
					//Converting miles to kilometers by taking miles and passing it to the conversion method
					case 2:
					    System.out.println("");
						System.out.println("Enter miles:");
						double miles = in.nextDouble();
						System.out.println("");
						System.out.printf("%.2f miles = %.4f km\n", miles, milesToKilometers(miles));
						System.out.println("");
						break;
					//Taking a GPA integer input and passing that to the appropriate method
					case 3:
					    System.out.println("");
						System.out.println("Enter your GPA:");
						double gradePointAverage = in.nextDouble();
						System.out.println("");
						System.out.println(graduateWithHonors(gradePointAverage));
						break;
					//Exiting the program, no default option
					case 9:
					    System.out.println("");
						System.out.println("Thank you for using the program. Goodbye!");
						break;
				}
			}
			
			//Clossing the scanner object
			in.close();
		}
		
		//Displaying menu with options
		public static void displayMenu()
		{
		    	System.out.println("Menu:\n" + "1: Convert cubic feet to U.S. bushels\n"
			 	+ "2: Convert miles to kilometers\n" + "3: Determine graduation title with honors\n"
			 	+ "9: Exit program");
		}

		//Ensuring that nothing can be entered except for the options offered from the menu, a string or double will error out
		public static int inputValidation(Scanner input)
		{
		    int continueUh = 0;
		    continueUh = input.nextInt();
		    
			while(continueUh != 1 && continueUh != 2 && continueUh != 3 && continueUh != 9)
			{
				System.out.println("I'm afraid that's off the menu, please choose from whats available");
				//Prompt user with menus

				displayMenu();

				//Take input from user and assign the value to continue
			
				continueUh = input.nextInt();
			}
			
			return continueUh;

		}

		//Using a given constant and the earlier prompted cubic feet to convert to bushels and return the value
		public static double cubicFeetToUSBushels(double cubicFeet)
		{	
			final double BUSHELSPERCUBICFOOT = 0.803564;
			double bushels = cubicFeet * BUSHELSPERCUBICFOOT;
			return bushels;
		}

		//Using a given constant and the earlier prompted miles to convert to kilometers an return the value
		public static double milesToKilometers(double miles)
		{
			final double KILOMETERSPERMILE = 1.60934;
			double kilometers = miles * KILOMETERSPERMILE;
			return kilometers;
		}

			//Taking the GPA integer prompted earlier and using a if-else-if ladder to assign a string
			//to a variable and return the value to be printed in the main method
	    	public static String graduateWithHonors(double gradePointAverage)
	    	{

	        	String announcement = "";
	        
	        	if (gradePointAverage < 3.5)
	        	{
	        		announcement = "I'm sorry, you did not graduate with honors.\n";
	        	}
	        	else if(gradePointAverage >= 3.5 && gradePointAverage <= 3.7)
	        	{
	        		announcement = "Congratulations, you have graduated Cum Laude!\n";
	        	}       
	        	else if(gradePointAverage >= 3.8 && gradePointAverage <= 3.9)
	        	{
	        		announcement = "Congratulations, you have graduated Magna Cum Laude!\n";
	        	}       
	        	else if(gradePointAverage >= 4.0)
	        	{
	        		announcement = "Congratulations, you have graduated Summa Cum Laude!\n";
	        	}
	        
	        	return announcement;
		
		}

	}
