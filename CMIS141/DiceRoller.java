package edu.umgc.cmis141.teasdale.methods;

import java.util.Scanner;
import java.util.Random;

public class DiceRoller {

	public static void main(String[] args)
	{
		
		//Creating variables and scanner instance
		int rollAmount = 0, diceType = 0, continueUh = 0;

		Scanner in = new Scanner(System.in);
		
		//Prompting user to see if they want to enter program or exit without entering data
		//In future programs I will make this a method to keep main method clean
		System.out.println("Would you like to roll some dice?");
		System.out.println("Enter '1' to play");
		System.out.println("Enter '2' to exit");
		
		//calling the continue validation method and checking to see if input is acceptable before assigning to continueUh
		continueUh = continueValidation(in);

		//Continue game loop
		while(continueUh != 2)
		{
			//Displaying dice types by using a method
			diceTypeMenu();

			//calling the dice validation method and checking to see if input is acceptable before assigning it to diceType
			diceType = diceValidation(in);		

			//Prompting user to roll dice x amount of times
			System.out.println("How many times would you like to roll your " + diceType + "-sided die");

			rollAmount = in.nextInt();

			//Since this amount of rolls is known, I'll use a for loop to iterate through each roll
			for(int i = 0; i < rollAmount; i++)
			{
				//Every time a "dice is rolled" the randomNumber method generate a number with an upper limit based on the
				//Type of die being rolled, then the result is printed out with a nice string.
				System.out.println("You rolled " + randomNumber(diceType, rollAmount) + "!");
			}

			//Displaying a want to play again menu, separate menu from the initial do you want to play menu at the top
			displayAgain();
			continueUh = continueValidation(in);
		}

		System.out.println("Thank you for running my program, have a good day!");

		//Closing out scanner object
		in.close();
	}

	/*
	 * This method displays the 'Want to play again?' menu
	 */
	public static void displayAgain()
	{
		System.out.println("Would you like to roll again?");
		System.out.println("Enter '1' to continue");
		System.out.println("Enter '2' to exit");
	}

	/*
	 * This method ensures only a 1 or a 2 can be passed to the continueUh variable
	 */
	public static int continueValidation(Scanner input)
	{
		int continueUh = 0;
		continueUh = input.nextInt();

		while(continueUh != 1 && continueUh != 2)
		{
			System.out.println("I'm afraid that wasn't an option");
			displayAgain();

			continueUh = input.nextInt();
		}

		return continueUh;
	}

	/*
	 * This method shows the main menu for choosing dice type
	 */
	public static void diceTypeMenu()
	{
		System.out.println("Welcome! Please choose from the available dice below.");
		System.out.println("Enter '20' to roll a 20-sided die");
		System.out.println("Enter '12' to roll a 12-sided die");
		System.out.println("Enter '10' to roll a 10-sided die");
		System.out.println("Enter '8' to roll an 8-sided die");
		System.out.println("Enter '6' to roll a 6-sided die");
		System.out.println("Enter '4' to roll a 4-sided die");
	}

	/*
	 * This method ensures that only the specific dice type could be used, I thought about putting an exit option in here
	 * but I decided to make it a separate variable.
	 */
	public static int diceValidation(Scanner input)
	{
		int diceType = 0;
		diceType = input.nextInt();

		while(diceType != 20 && diceType != 12 && diceType != 10 && diceType != 8 && diceType != 6 && diceType != 4)
		{
			System.out.println("I'm sorry, that is not one of the die we offer to roll, please choose from the menu.");

			diceTypeMenu();
			diceType = input.nextInt();
		}

		return diceType;
	}

	/*
	 * This is my method that takes the diceType, and generates a number up to that upper limit.
	 */
	public static int randomNumber(int diceType, int rollAmount)
	{
	    //System.out.println(diceType);
	    //System.out.println(rollAmount);
	    
		Random player = new Random();
		
		//Adding 1 to its 1-20 and not 0-19
		int diceRoll = player.nextInt(diceType) + 1;

		return diceRoll;
		
	}
	
}
