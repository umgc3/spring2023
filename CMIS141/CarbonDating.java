package edu.umgc.cmis141.teasdale.week2inputandarithmetic;

import java.lang.Math;
import java.util.Scanner;

public class CarbonDating {

	public static void main(String[] args) {
		
	//Creating variables

	double percentCarbon, halfLife, timeSinceDeath;
	final double N_ZERO = 100.0; //N0 refers to the hypothetical C12 and C14 percentage of a tree from today
	final double RATE_OF_DECAY = -0.693; //Rate of decay for C14

	//Creating scanner

	Scanner in = new Scanner(System.in);

	//Asking for inputs

	System.out.println("You have recenlty found the branch of an old tree,\n"
			+ " comparing it to the same species still around today, it's\n"
			+ " noticed that it only has a certain percentage of Carbon 14\n"
			+ " (please enter a number between 1 and 100): ");
	percentCarbon = in.nextDouble();
	System.out.println("After finding and entering the percent of carbon\n"
			+ " into your formula, the only other step is to add the\n"
			+ " half-life of Carbon 14 (The halflife of C14 is 5730,\n"
			+ " but any number positive can be entered): ");
	halfLife = in.nextDouble();

	//Calculating time since death using carbon dating formula

	timeSinceDeath = ((Math.log(percentCarbon/N_ZERO))/RATE_OF_DECAY)*halfLife;

	//Outputting Results

	System.out.println("This piece of wood is " + (int)timeSinceDeath + " years old");

	//Closing scanner object

	in.close();	

	}
}
