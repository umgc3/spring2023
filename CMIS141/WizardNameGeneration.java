package edu.umgc.cmis141.teasdale.arrays;

import java.util.Scanner;
import java.util.Random;

public class WizardNameGeneration 
{

	public static void main(String[] args)
	{
		//Creating variables and arrays within main
		
		String usersCopy[], firstNamesCopy[], middleNamesCopy[], lastNamesCopy[];

		String accountChoice, genderChoice, nameLength, firstName = "", middleName = "", lastName = "";
		
		//New scanner object

		Scanner in = new Scanner(System.in);

		//Passing scanner object to accountValidation method
		
		accountChoice = accountValidation(in);
		
		//While the user doesn't want to stop will continue going
		//Ensuring you can only type Y/N or Yes/No

		while(!(accountChoice.equals("No")) && !(accountChoice.equals("N")))
		{
			//Passing scanner object to accountCreation method
			
			usersCopy = accountCreation(in);
			
			//For every index (account) in userCopy, do these things

			for(int i = 0; i < usersCopy.length; i++)
			{
				
				//Filler text to space out different parts of logic
				
			    System.out.println("");
			    System.out.println("Prepare yourself " + usersCopy[i] + ", you have some choices to make: ");
			    System.out.println("");
			        
			    //Passing scanner object to genderValidation method
			    //Ensuring you can either type M/F or Masculine/Feminine
			    
				genderChoice = genderValidation(in);
				
				//If statement to determine whether first names should reference masculine or feminine first names array

				if(genderChoice.equals("Masculine") || genderChoice.equals("M"))
				{
					firstNamesCopy = maleFirstNames();
					middleNamesCopy = middleNames();
					lastNamesCopy = lastNames();
					
					//Passing scanner object to nameLengthValidation method
					//Making sure that your response of the if statement down below is what I want it to be

					nameLength = nameLengthValidation(in);
					
					//In Wizard 101 you can choose to only have a first name, a first and last name, or a first and two last names
					//This is me coming up with a system to replicate that
					
					//The string name variable is set to a random number generated to the upper bound of the array length
					//This is then passed to a reference of an array, which becomes the index to determine a random name

					if (nameLength.equals("Common"))
					{
						firstName = firstNamesCopy[randomNumber(firstNamesCopy.length)];
					}
					else if (nameLength.equals("Noble"))
					{

						firstName = firstNamesCopy[randomNumber(firstNamesCopy.length)];
						middleName = middleNamesCopy[randomNumber(middleNamesCopy.length)];

					}
					else if (nameLength.equals("Royalty"))
					{

						firstName = firstNamesCopy[randomNumber(firstNamesCopy.length)];
						middleName = middleNamesCopy[randomNumber(middleNamesCopy.length)];
						lastName = lastNamesCopy[randomNumber(lastNamesCopy.length)];

					}
					
					//Final result of your name

					System.out.println("");
					System.out.printf("Your wizard name is: %s %s %s", firstName, middleName, lastName);
					System.out.println("");

				}
				else
				{
					
					//Same thing but for feminine names instead
					
					firstNamesCopy = femaleFirstNames();
					middleNamesCopy = middleNames();
					lastNamesCopy = lastNames();

					nameLength = nameLengthValidation(in);

					if (nameLength.equals("Common"))
					{
						firstName = firstNamesCopy[randomNumber(firstNamesCopy.length)];
					}
					else if (nameLength.equals("Noble"))
					{

						firstName = firstNamesCopy[randomNumber(firstNamesCopy.length)];
						middleName = middleNamesCopy[randomNumber(middleNamesCopy.length)];

					}
					else if (nameLength.equals("Royalty"))
					{

						firstName = firstNamesCopy[randomNumber(firstNamesCopy.length)];
						middleName = middleNamesCopy[randomNumber(middleNamesCopy.length)];
						lastName = lastNamesCopy[randomNumber(lastNamesCopy.length)];

					}
                    
                    System.out.println("");
					System.out.printf("Your wizard name is: %s %s %s", firstName, middleName, lastName);


				}
			}
			
			//Would you like to continue?
			//Passing scanner object to continuationValidation method
			accountChoice = continuationValidation(in);
		}

		//Final statement, as a check to see when the loop ends
		System.out.println("Thank you for using my program, have a great day!");

		//Closing scanner object to prevent memory issues
		in.close();
	}

	//Several print statements stored in methods to easily call
	public static void accountMenu()
	{
		System.out.print("Would you like to make an account? (Yes/No)");
	}

	public static void genderMenu()
	{
		System.out.print("Would you like a masculine first name or feminine first name? (Masculine/Feminine)");
	}

	public static void nameLengthMenu()
	{
		System.out.println("These are the available name types: ");
		System.out.println("Common: A first name");
		System.out.println("Noble: A first and last name");
		System.out.println("Royalty: A first, middle, and last name");
		System.out.println("Please enter your prefered type");
	}

	public static void continuationMenu()
	{
	    System.out.println("");
	    System.out.println("");
		System.out.print("Would you like to make more accounts? (Yes/No)");
	}

	//Several validation loops to ensure that you type but only a couple options
	public static String accountValidation(Scanner input)
	{

		String accountChoice = "", accountInput = "";

		accountMenu();

		accountInput = input.nextLine();
		System.out.println("");

		accountChoice = accountInput.substring(0,1).toUpperCase() + accountInput.substring(1).toLowerCase();

		while(!(accountChoice.equals("Yes")) && !(accountChoice.equals("Y")) && !(accountChoice.equals("No")) && !(accountChoice.equals("N")))
		{
			System.out.println("Sorry, I don't recognize your input");

			accountMenu();

			accountInput = input.nextLine();
			System.out.println("");

			accountChoice = accountInput.substring(0,1).toUpperCase() + accountInput.substring(1).toLowerCase();

		}

		return accountChoice;
	}

	public static String genderValidation(Scanner input)
	{
		String genderChoice = "", genderInput = "";

		genderMenu();

		genderInput = input.nextLine();
		System.out.println("");

		genderChoice = genderInput.substring(0,1).toUpperCase() + genderInput.substring(1).toLowerCase();

		while(!(genderChoice.equals("Masculine")) && !(genderChoice.equals("M")) && !(genderChoice.equals("Feminine")) && !(genderChoice.equals("F")))
		{
			System.out.println("Sorry, I don't recognize your input");

			genderMenu();

			genderInput = input.nextLine();
			System.out.println("");

			genderChoice = genderInput.substring(0,1).toUpperCase() + genderInput.substring(1).toLowerCase();
		}

		return genderChoice;
	}

	public static String nameLengthValidation(Scanner input)
	{
		String nameLengthChoice = "", nameLengthInput = "";

		nameLengthMenu();
		System.out.println("");

		nameLengthInput = input.nextLine();

		nameLengthChoice = nameLengthInput.substring(0,1).toUpperCase() + nameLengthInput.substring(1).toLowerCase();

		while(!(nameLengthChoice.equals("Common")) && !(nameLengthChoice.equals("Noble")) && !(nameLengthChoice.equals("Royalty")))
		{
			System.out.println("Sorry, I don't recognize your input");

			nameLengthMenu();

			nameLengthInput = input.nextLine();
			System.out.println("");

			nameLengthChoice = nameLengthInput.substring(0,1).toUpperCase() + nameLengthInput.substring(1).toLowerCase();

		}

		return nameLengthChoice;
	}

	public static String continuationValidation(Scanner input)
	{
		String continuationChoice = "", continuationInput = "";

		continuationMenu();

		continuationInput = input.nextLine();
		System.out.println("");

		continuationChoice = continuationInput.substring(0,1).toUpperCase() + continuationInput.substring(1).toLowerCase();

		while(!(continuationChoice.equals("Yes")) && !(continuationChoice.equals("Y")) && !(continuationChoice.equals("No")) && !(continuationChoice.equals("N")))
		{
			System.out.println("Sorry, I don't recognize your input");

			continuationMenu();

			continuationInput = input.nextLine();
			System.out.println("");

			continuationChoice = continuationInput.substring(0,1).toUpperCase() + continuationInput.substring(1).toLowerCase();

		}

		return continuationChoice;

	}

	//Method for seeing how many accounts you want to create, making a string array based off of that number
	//Then iterating through each index of that array and adding a name to that index
	//Returns a reference to that array to be assigned to the usersCopy[] variable
	public static String[] accountCreation(Scanner input)
	{
		String[] users;
		
		int numAccounts;

		System.out.println("How many accounts would you like to make? ");
		
		numAccounts = input.nextInt();
		input.nextLine();

		users = new String[numAccounts];

		for(int i = 0; i < numAccounts; i++)
		{
			if(numAccounts == 1)
			{
			    System.out.println("");
				System.out.println("What would you like to name your account?");
				users[i] = input.nextLine();
			}
			else
			{	
			    System.out.println("");
				System.out.println("What would you like to name account number " + (i + 1));
				users[i] = input.nextLine();
			}
		}

		return users;
	}

	//Takes array length as the parameter, then generates a number based off of that as an upper bound (-1 as arrays start at 0)
	//Returns that random number to be passed to an array as an index number
	public static int randomNumber(int arrayLength)
	{
		Random rand = new Random();
	
		int randomIndex;
		
		randomIndex = rand.nextInt(arrayLength) - 1;

		return randomIndex;
	}

	//Series of long string arrays holding a list of all possible names in Wizard 101
	//I will pass the variable referencing the array to be assigned to an array variable in main
	
	public static String[] maleFirstNames()
	{
		//String[] maleFirstNames = new String[154];

		String[] maleFirstNames = {"Aaron", "Adam", "Adrian", "Aedan", "Alejandro", "Alex", "Alexander", "Allan", "Alric", "Andrew", "Angel", "Angus",
		"Anthony", "Antonio", "Arlen", "Artur", "Austin", "Belgrim", "Benjamin", "Blaine", "Blake", "Blaze", "Boris", "Bradley", "Bradley", "Brahm", "Brand",
		"Brandon", "Brian", "Caleb", "Caley", "Cameron", "Carlos", "Cass", "Charles", "Chase", "Chris", "Christo", "Christopher", "Cody", "Cole", "Colin", "Connor",
		"Corwin", "Cowan", "Coyle", "Dakota", "Daniel", "David", "Devin", "Digby", "Dolan", "Dugan", "Duncan", "Dustin", "Dylan", "Edward", "Elie", "Elijah", "Eric",
		"Ethan", "Evan", "Finnigan", "Flint", "Fred", "Gabriel", "Galen", "Garrett", "Gavin", "Gilroy", "Gorman", "Hunter", "Ian", "Isaac", "Isaiah", "Jack", "Jacob",
		"James", "Jared", "Jason", "Jeffrey", "Jeremy", "Jesse", "John", "Jonathan", "Jordan", "Jose", "Joseph	", "Joshua", "Juan", "Justin", "Kane", "Karic", "Keelan",
		"Keller", "Kenneth", "Kevin", "Kieran", "Kyle", "Lail", "Liam", "Logan", "Lucas", "Luis", "Luke", "Malorn", "Malvin", "Marcus", "Mark", "Mason", "Matthew",
		"Michael", "Miguel", "Mitchell", "Morgrim", "Mycin", "Nathan", "Nathaniel", "Nicholas", "Noah", "Oran", "Padric", "Patrick", "Paul", "Quinn", "Reed", "Richard",
		"Robert", "Rogan", "Ronan", "Ryan", "Samuel", "Scot", "Sean", "Seth", "Sloan", "Stephen", "Steven", "Talon", "Tanner", "Tarlac", "Taylor", "Thomas", "Timothy",
		"Travis", "Trevor", "Tristan", "Tyler", "Valdus", "Valerian", "Valkoor", "William", "Wolf", "Zachary"};

		return maleFirstNames;
	}

	public static String[] femaleFirstNames()
	{
		//String[] femaleFirstNames = new String[143];
		
		String[] femaleFirstNames = {"Abigail", "Alexandra", "Alexandria", "Alexis", "Alia", "Alicia", "Allison", "Alura", "Alyssa", "Amanda", "Amber", "Amy", "Andrea",
		"Angela", "Anna", "Ashley", "Autumn", "Bailey", "Brecken", "Brianna", "Brittany", "Brooke", "Brynn", "Caitlin", "Calamity", "Caroline", "Cassandra", "Catherine",
		"Chelsea", "Cheryl", "Cheyenne", "Christina", "Cori", "Courtney", "Danielle", "Darby", "Deirdre", "Delaney", "Destiny", "Devin", "Diana", "Donna", "Elizabeth",
		"Ellie", "Emily", "Emma", "Emmaline", "Erica", "Erin", "Esmee", "Fallon", "Fiona", "Gabrielle", "Genevieve", "Ginelle", "Grace", "Haley", "Hannah	", "Heather",
		"Iridian", "Isabella", "Jacqueline", "Jasmine", "Jenna", "Jennifer", "Jessica", "Jordan", "Julia", "Kaitlyn", "Katherine", "Katie", "Kayla", "Keena", "Keira",
		"Kelly", "Kelsey", "Kestrel", "Kiley", "Kimberly", "Kristen", "Kymma", "Laura", "Lauren", "Leesha", "Lenora", "Lindsey", "Llewella", "Mackenzie", "Madeline",
		"Madison", "Maria", "Mariah", "Marissa", "Mary", "Megan", "Melissa", "Michelle", "Mindy", "Miranda", "Moira", "Molly", "Monica", "Morgan", "Myrna", "Natalie",
		"Neela", "Nicole", "Nora", "Olivia", "Paige", "Rachel", "Rebecca", "Roslyn", "Rowan", "Ryan", "Rylee", "Sabrina", "Saffron", "Samantha", "Sarah", "Sarai",
		"Savannah", "Scarlet", "Sestiva", "Shanna", "Shannon", "Shawna", "Shelby", "Sierra", "Sophia", "Stephanie", "Suri", "Sydney", "Tabitha", "Tara", "Taryn", "Tasha",
		"Tatiana", "Tavia", "Taylor", "Terri", "Tiffany", "Vanessa", "Victoria"};

		return femaleFirstNames;
	}

	public static String[] middleNames()
	{
		//String[] middleNames = new String[85];

		String[] middleNames = {"Angle", "Anvil", "Ash", "Battle", "Bear", "Blue", "Boom", "Crow", "Daisy", "Dark", "Dawn", "Day", "Death", "Dragon", "Drake", "Dream",
		"Dune", "Dusk", "Earth", "Emerald", "Fairy", "Fire", "Foe", "Frog", "Frost", "Ghost", "Giant", "Gold", "Golden", "Green", "Griffin", "Hawk", "Hex", "Ice", "Iron",
		"Jade", "Legend", "Life", "Light", "Lion", "Lotus", "Mist", "Moon", "Myth", "Night", "Ogre", "Owl", "Pearl", "Pixie", "Rain", "Rainbow", "Raven", "Red", "Rose",
		"Ruby", "Sand", "Sea", "Shadow", "Silver", "Skull", "Sky", "Soul", "Sparkle", "Spell", "Spirit", "Sprite", "Star", "Storm", "Story", "Strong", "Summer", "Sun",
		"Swift", "Tale", "Thunder", "Titan", "Troll", "Unicorn", "Water", "Wild", "Willow", "Wind", "Winter", "Wyrm"};

		return middleNames;
	}

	public static String[] lastNames()
	{
		//String[] lastNames = new String[79];

		String[] lastNames = {"Bane", "Blade", "Blood", "Bloom", "Blossom", "Brand", "Breaker", "Breath", "Breeze", "Bright", "Bringer", "Caller", "Caster", "Catcher",
		"Cloud", "Coin", "Crafter", "Dreamer", "Dust", "Eyes", "Finder", "Fist", "Flame", "Flower", "Forge", "Fountain", "Friend", "Garden", "Gem", "Giver", "Glade",
		"Glen", "Grove", "Hammer", "Hand", "Haven", "Head", "Heart", "Horn", "Hunter", "Leaf", "Mancer", "Mask", "Mender", "Pants", "Petal", "Pyre", "Rider", "River",
		"Runner", "Shade", "Shard", "Shield", "Singer", "Slinger", "Smith", "Song", "Spear", "Staff", "Stalker", "Steed", "Stone", "Strider", "Sword", "Tail", "Talon",
		"Tamer", "Thief", "Thistle", "Thorn", "Vault", "Walker", "Ward", "Weave", "Weaver", "Whisper", "Wielder", "Wraith"};

		return lastNames;
	}

	
}
