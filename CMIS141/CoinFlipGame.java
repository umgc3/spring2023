package edu.umgc.cmis141.teasdale.weekfiveforloop;

import java.util.Scanner;
import java.util.Random;


public class CoinFlipGame {

	public static void main(String[] args) {

		//Creating variables, a scanner object, and a random object
		int coinToss, randomCoin, max = 2, min = 1;
		String temp, guess;

		Scanner in = new Scanner(System.in);

		Random coin = new Random();

		//Prompt user for input

		System.out.println("How many times would you like to flip a coin?");
		coinToss = in.nextInt();
		in.nextLine();

		//First loop to generate a number between 1 and 2 and assign it to randomCoin

		for(int x = 0; x<coinToss; x++)
		{
			randomCoin = coin.nextInt((max - min) + 1) + min;

			//System.out.println(randomCoin); Used for testing to check number generations and if statement below

			//Second loop to have the user guess and give the result
		
			for(int y = x; y<=x; y++)
			{
				System.out.println("");
				System.out.println("Heads or Tails");
				temp = in.nextLine();
				guess = temp.substring(0,1).toUpperCase() + temp.substring(1);

				if(guess.equals("Heads") && randomCoin == 1)
				{
					System.out.println("You're right! The coin landed on " + guess);
				}
				else if (guess.equals("Tails") && randomCoin == 1)
				{
					System.out.println("Not quite, the coin landed on " + guess);
				}
				else if (guess.equals("Tails") && randomCoin == 2)
				{
					System.out.println("You're right! The coin landed on " + guess);
				}
				else if (guess.equals("Heads") && randomCoin == 2)
				{
					System.out.println("Not quite, the coin landed on " + guess);
				}
				else
				{
					System.out.println("Sorry that wasn't 'Heads' or 'Tails', lose a turn.");
				}
			}

		}
		
		System.out.println("Have a good day!");
		in.close();
		
	}

}
