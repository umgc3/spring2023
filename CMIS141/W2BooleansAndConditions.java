package edu.umgc.cmis141.teasdale.assignmenttwoconditions;

import java.util.Scanner;

public class BooleansAndConditions {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Declaring variables and creating scanner

		double numOne, numTwo, result;
		char operator;
		
		Scanner in = new Scanner(System.in);

		//Prompting user for two numbers and a character (operator)
		//Using doubles as the prompt says ANY number between 200 and 1000 ;), also it will prevent integer division

		System.out.print("Please enter two numbers between 200 and 1000\n"
		+ "and an operation symbol ('+', '-', '*', '/')\n"
		+ "with spaces between each input: ");

		//Assigning, in order, input from user into variables

		numOne = in.nextDouble();
		numTwo = in.nextDouble();
		operator = in.next().charAt(0);

		//Using a switch statement for performing an operation based on the character stored in 'operator'

		switch (operator)
		{
		case '+':
			result = numOne + numTwo;
			System.out.printf("Evaluation: %.2f %c %.2f = %.2f\n", numOne,operator, numTwo, result);	//formatting output using printf so doubles aren't excessively long
			break;
		case '-':
			result = numOne - numTwo;
			System.out.printf("Evaluation: %.2f %c %.2f = %.2f\n", numOne,operator, numTwo, result);
			break;
		case '*':
			result = numOne * numTwo;
			System.out.printf("Evaluation: %.2f %c %.2f = %.2f\n", numOne,operator, numTwo, result);
			break;
		case '/':
			result = numOne / numTwo; 
			System.out.printf("Evaluation: %.2f %c %.2f = %.2f\n", numOne,operator, numTwo, result);
			break;
		default:
			System.out.println("Sorry, " + operator + " is not a valid operation symbol"); //error handling for unexpected characters in the operator variable
			break;
		}

		//closing scanner class

		in.close();
		
	}

}
