package edu.umgc.cmis141.teasdale.weekfournestedloops;

import java.util.Scanner;

public class ForLoops {

	public static void main(String[] args)
	{

		//Creating variables and scanner object

		int numRows;

		Scanner in = new Scanner(System.in);

		//Prompting user for number of rows in triangle

		System.out.println("How many rows would you like in your triangle?");
		numRows = in.nextInt();

		//Creating a loop to filter out input less than or equal to 0
		
		while(numRows <= 0)
		{
			System.out.println("Sorry I dont think I can make a triangle with " + numRows 
			+ "\n rows, please try another number");
			numRows = in.nextInt();
		}
		
		//Creating nested for loops to create a triangle, size base off of numRows

		for(int x = 1; x <= numRows; x++)
		{
			System.out.println();
			for(int y = 1; y <= x; y++)
			{
				
				//formatting output
				System.out.printf("%4d", y);
			}
		}

		//Closing scanner object to prevent memory issues
		in.close();

	}	
}

