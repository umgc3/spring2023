package edu.umgc.cmis141.teasdale.weekfourwhileloop;

import java.util.Scanner;
import java.util.Random;

public class NumberGuessGame
{
    public static void main(String[] args) {


		//Creating variables and scanner

		int randomNum, guess = 0, playAgain = 1;

		Random generator = new Random();

		Scanner in = new Scanner(System.in);

		//starting loop for game

		while(playAgain == 1)
		{	
			//generate a random number
		
			randomNum = generator.nextInt(10) + 1;

		
			//Asking for user input
		
			System.out.println("I'm thinking of a number between 1 & 10, can you guess?");
			guess = in.nextInt();

			//loop for guessing number
		
			while(guess != randomNum)
			{
				
				System.out.println("Not quite, try again");
				guess = in.nextInt();

			}

			//Number was correct, continue?
		
			System.out.println("");
			System.out.println("Well done! you found that my number was: " + randomNum);
			System.out.println("Would you like to play again? Enter 1 to continue or 2 to Exit: ");
			playAgain = in.nextInt();

		}

		//No continue, bye

		in.close();
		System.out.println("Have a great day!");
	
    }
}

