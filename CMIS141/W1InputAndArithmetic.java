package edu.umgc.cmis141.teasdale.assingmentoneinputoutput;

//Author: Parker Teasdale
//Date: 03/22/2023
//Purpose: Taking several inputs from user within context of a grocery store, and outputting totals before and after discount

//Importing scanner class

import java.util.Scanner;

public class InputAndArithmetic {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		//Establishing variables

		String productDescription;
		int customerID, quantity;
		double unitPrice, discount, totalPriceNoDiscount, totalPriceDiscount;

		//Creating scanner
		Scanner in = new Scanner(System.in);

		//Asking user for input and adding them to variables

		System.out.println("What is your customer ID? ");
		customerID = in.nextInt();

		System.out.println("What is the exact Unit Price to two decimals?");
		unitPrice = in.nextDouble();

		System.out.println("What is the quantity?");
		quantity = in.nextInt();
		
		
		//Preventing integer to string bug
		in.nextLine();

		System.out.println("What is a rough description of the product?");
		productDescription = in.nextLine();

		System.out.println("What is your discount in the from of 0.xx");
		discount = in.nextDouble();

		//Calculating price with and without discounts

		totalPriceNoDiscount = unitPrice * quantity;
		totalPriceDiscount = totalPriceNoDiscount - (totalPriceNoDiscount * discount);

		//Outputting inputs and calculated totals5
		
		System.out.println("");
		System.out.println("ORDER DATA:");
		System.out.println("Customer id: " + customerID);
		System.out.printf("Unit Price: %.2f\n", unitPrice);
		System.out.println("Quantity: " + quantity);
		System.out.println("Product Description: " + productDescription);
		System.out.printf("Discount: %.2f\n", discount);
		System.out.println("");
		System.out.printf("Order total before discount: %.2f\n", totalPriceNoDiscount);
		System.out.printf("Order total after discount: %.2f\n", totalPriceDiscount);

		//Closing scanner

		in.close();

		
	}

}
