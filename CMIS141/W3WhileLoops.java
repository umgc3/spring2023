package edu.umgc.cmis141.teasdale.assignmentthreeloops;

import java.util.Scanner;

public class WhileLoops
{
	public static void main(String[] args)
	{
		//Creating variables and scanner
		String gamerName;
		String input = null;
		String continueUh = " ";
		int levelOneXP, levelTwoXP, levelThreeXP, engagementScore, finalScore;
		
		//Constant for XP bonuses by level
		final double LEVEL_ONE_XP_BONUS = 1.20;
		final double LEVEL_TWO_XP_BONUS = 1.30;
		final double LEVEL_THREE_XP_BONUS = 1.50;
		final double ENGAGEMENT_SCORE_BONUS = 1.60;

		Scanner in = new Scanner(System.in);
		
		//Starting game loop

		while (input == null || !(continueUh.equals("No")))
		{

			//Welcoming user and asking if they want to continue, can exit without entering data
			
			System.out.println("Welcome to the Total XP calculation program!");
			System.out.println("Do you want to enter a gamer's data? (Yes/No)");
			input = in.nextLine();
			
			//Taking the first character of the entered string, capitalizing it, then adding it to the second half

			continueUh = input.substring(0,1).toUpperCase() + input.substring(1);

			//While user wishes to continue I will keep prompting them for info 

			while (continueUh.equals("Yes"))
			{

				//Prompting user for their name and scores
				System.out.println("Enter the gamer's name: ");
				gamerName = in.nextLine();

				System.out.println("Enter the gamer's level XP scores\n"
				+"separated by space: L1 L2 L3 ES");
				levelOneXP = in.nextInt();
				levelTwoXP = in.nextInt();
				levelThreeXP = in.nextInt();
				engagementScore = in.nextInt();
				in.nextLine();
				
				//Input validation so that the user has to enter a number between 10 and 100, and also has to be a multiple of 5

				while(!(levelOneXP <=100 && levelOneXP >= 10 && levelOneXP % 5 == 0)
				|| !(levelTwoXP <=100 && levelTwoXP >= 10 && levelTwoXP % 5 == 0)
				|| !(levelThreeXP <=100 && levelThreeXP >= 10 && levelThreeXP % 5 == 0)
				|| !(engagementScore <=100 && engagementScore >= 10 && engagementScore % 5 == 0))
				{
					System.out.println("Sorry, One or multiple of the numbers you input were not between 10 & 100 and in intervals of 5");

					System.out.println("Enter the gamer's level XP scores\n"
					+"separated by space: L1 L2 L3 ES");
					levelOneXP = in.nextInt();
					levelTwoXP = in.nextInt();
					levelThreeXP = in.nextInt();
					engagementScore = in.nextInt();
					in.nextLine();
				}
				
				//Adding up all the levels with their respective bonus constants

				finalScore = (int)((levelOneXP * LEVEL_ONE_XP_BONUS) + (levelTwoXP * LEVEL_TWO_XP_BONUS) + (levelThreeXP * LEVEL_THREE_XP_BONUS) + (engagementScore * ENGAGEMENT_SCORE_BONUS));

				//Outputing final results
				
				System.out.println("Gamer name = " + gamerName);
				System.out.println("L1: " + levelOneXP);
				System.out.println("L2: " + levelTwoXP);
				System.out.println("L3: " + levelThreeXP);
				System.out.println("ES: " + engagementScore);

				System.out.println("Final XP score with bonuses: " + finalScore);

				//Setting input to null for every loop so I can prompt user for if they wish to continue
				
				input = null;		
				continueUh = " ";
			}
		}

		//Closing scanner object
		
		in.close();

		//Print out so I can see when loop ends, also it's nice
		
		System.out.println("Have a good day!");
		
	}

}
