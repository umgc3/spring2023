package edu.umgc.cmis141.teasdale.weekfournestedloops;

import java.util.Scanner;
import java.lang.Math;

public class ForLoops2 
{

	public static void main(String [] args)
	{

		//Creating variables and scanner object

		int startingNumber;

		Scanner in = new Scanner(System.in);

		//Asking user for input
		
		System.out.println("What would you like your starting number to be?,\n"
				+ "Please enter a number greater than 0");
		startingNumber = in.nextInt();
		
		//Creating a while loop to filter out inputs less than 1
		
		while(startingNumber <=0 )
		{
			System.out.println("Sorry, please enter a number greater than 0");
			startingNumber = in.nextInt();
		}

		
		//creating two for loops that will create a rectangle of numbers the size of startingNumber * 2 - 1
		//that will iterate down to 1 and back up to the startingNumber

		for(int row = -(startingNumber - 1); row <= (startingNumber-1); row++)
		{
			for(int col = -(startingNumber -1); col <= (startingNumber-1); col++)
			{
                
						//Creating variables for the absolute value of the variables established in the loops above
                		int modifiedRow = Math.abs(row) + 1;
                		int modifiedCol = Math.abs(col) + 1;
                		
                		//Checking to see whether the absolute value of Col is bigger or equal to the absolute value of Row
                		//Then assigning one or the other value to printValue
                		int printValue = modifiedCol >= modifiedRow ? modifiedCol : modifiedRow;
                		
                		//Printing out the value of printValue, which over many iterations will build the rectangle output
				System.out.printf("%4d", printValue);
			}

			System.out.println();
		}
		
		//closing the scanner object to avoid memory issues
		in.close();
	}

	
}
