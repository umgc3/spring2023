package edu.umgc.cmis141.teasdale.arrays;

//Parker Teasdale
//05/02/2023
//Will take in an n number of teams, ask for a name and score for each team and store them into arrays
//Displays the names of your teams and the associates scores
//Uses methods to return the lowest and highest scores and their assoicated teams

import java.util.Scanner;

public class Arrays
{

	public static void main(String[] args)
	{
		//Creating scanner object
		
		Scanner in = new Scanner(System.in);	

		//Prompting user for number of teams
		
		System.out.print("What number of teams do you want to enter? ");

		//Small validation loop against negative numbers
		
		int numTeams = in.nextInt();
		
			while(numTeams < 0)
			{
				System.out.println("Sorry I don't think " + numTeams + " teams is possible, please try again");
				
				System.out.print("What number of teams do you want to enter? ");

				numTeams = in.nextInt();
			}
		
			//If you put 0 it will send you to the end of the program, as you don't wish to enter any teams
			
			if(numTeams != 0)
			{

			//Creating strings based off of the number of teams you wish to enter	
				
			String[] teamName = new String[numTeams];
		
			int[] teamScore = new int[numTeams];

			//Iterating through each index in the name and score array
			//With every iteration you assign a name and number to that index, starting at 0 and ending at your numTeams - 1
			
			for(int i = 0; i < numTeams; i++)
			{
				//prompting for name
				
		    	in.nextLine();
				System.out.println("");
				System.out.print("Enter the team's name: ");

				teamName[i] = in.nextLine();

				//prompting for an integer number, I give a recommended range
				
				System.out.println("");
				System.out.print("Enter the team's score (400-1000) ");

				teamScore[i] = in.nextInt();
			}
		
			System.out.println("");
			
			//After the arrays are assigned values at every index, I am now iterating through once again and printing the results
		
			for (int i = 0; i < numTeams; i++)
			{
		    	System.out.printf("%-20s %d", teamName[i], teamScore[i]);
		    	System.out.println("");
			}
			
			//Using methods to find the highest and lowest scores within the array, then returning the index of that value
			//Using this index in conjunction with arrays to print the names and scores
		
			System.out.println("");
			System.out.println("The " + teamName[highestIndex(teamScore)] + " have the highest score at " + teamScore[highestIndex(teamScore)] +
			"\nand The " + teamName[lowestIndex(teamScore)] + " have the lowest score at " + teamScore[lowestIndex(teamScore)]);
		}
			
		//Exit message to check for If conditional failure (0)

		System.out.println("");
		System.out.println("Have a good day!");
		
		//Closing scanner object
	
		in.close();

	}

	//Setting a variable called lowestScore to the very first index
	//Then checking to see if the value after is lower than that score
	//If it is I assign that value to the lowest score, and repeat as I iterate through the array
	//Once the lowest score is found I assign that index value (whatever value j is on while looping) and return that
	
	public static int lowestIndex(int[] teamScore)
	{
		int lowestScore = teamScore[0];
		int lowestIndex = 0;

		for (int j = 0; j < teamScore.length; j++)
		{
			if(teamScore[j] < lowestScore)
			{
				lowestScore = teamScore[j];
				lowestIndex = j;
				
			}
		}

		return lowestIndex;

	}

	//Setting a variable called highestScore to the very first index
	//Then checking to see if the value after is higher than that score
	//If it is I assign that value to the highest score, and repeat as I iterate through the array
	//Once the highest score is found I assign that index value (whatever value k is on while looping) and return that
	
	public static int highestIndex(int[] teamScore)
	{
		int highestScore = teamScore[0];
		int highestIndex = 0;

		for (int k = 0; k < teamScore.length; k++)
		{
			if(teamScore[k] > highestScore)
			{
				highestScore = teamScore[k];
				highestIndex = k;
			}
		}

		return highestIndex;

	}
	
}
