package edu.cmis141.teasdale.finalproject;
/*
 * Author: Parker Teasdale
 * Date: 05/09/2023
 * Description: Customer Class to describe a customer object
 */
public class Customer 
{
	//Attributes
	private String customerName;
	private int customerID;
	private double customerSale;

	//Constructor
	/*
	 * This constructor takes in three parameters, and uses the this. keyword to prevent shadowing
	 */
	public Customer(String customerName, int customerID, double customerSale)
	{
		this.customerName = customerName;
		this.customerID = customerID ;
		this.customerSale = customerSale;
	}

	//Methods
	/*
	 * Getters for each attribute so another class can use these
	 */
	
	public String getCustomerName()
	{
		return customerName;
	}

	public int getCustomerID()
	{
		return customerID;
	}

	public double getCustomerSale()
	{
		return customerSale;
	}
}

package edu.cmis141.teasdale.finalproject;

/*
 * Author:Parker Teasdale
 * Date: 05/09/2023
 * Description: A database system which you can create multiple customers, or only one customer, for each customer you input
 * a user ID and sales data. You can then choose to print out all customers and their data, find a customer based off a user
 * ID, or find users based on a range of sales data
 * 
 * The only thing that I didn't implement that I would want to, is a function to check if a input ID for a customer is a duplicate
 * and if so, then have the user asked for a new ID for that customer.
 */

import java.util.Scanner;

public class Store 
{
	public static void main(String[] args)
	{
		
		//Creating variables
		int mainMenuChoice = 0, numCustomers = 0, counter = 0, target = 0, low, high;

		//Creating my store array holding customer objects, maximum of 100.
		Customer store[] = new Customer[100];

		//Creating my scanner object
		Scanner in = new Scanner(System.in);

		//Passing scanner object to validation function, then assigning the output to a variable
		mainMenuChoice = mainMenuChoiceValidation(in);
		
		//Using a variable to start my main loop, until choosing the exit option in the main menu you will keep repeating
		while(mainMenuChoice != 9)
		{
			//Creating a switch statement for each choice in the main menu
			switch(mainMenuChoice)
			{
				case 1:
					//There cannot be more users than space in the array
					if(counter < 100)
					{
						//Passing scanner object to validation function, then assigning the output to a variable
						numCustomers = numCustomersValidation(in);
						
						//For every customer you want to create
						//Pass scanner input to account creation function, then store the output to an index in the array
						//I have a counter counting for every account that is made.
						for(int i = 0; i < numCustomers; i++)
						{
							store[counter] = newCustomer(in);
							counter++;
						}
					}
					else
					{
						System.out.println("Sorry there is no room for more customers");
					}

						break;

				case 2:
					//There cannot be more users than space in the array
					if(counter < 100)
					{
						//Pass scanner input to an account creation function, then store the output to an index in the array
						//Increment counter by 1 as this is only one account
						store[counter] = newCustomer(in);
						counter++;
					}
					else
					{
						System.out.println("Sorry there is no room for more customers");
					}

					break;

				case 3:
					//Passing the store object which references an array to a function that will print them out
					printCustomers(store);
					break;

				case 4:
					//Passing the store object, an integer names target, and the counter to a function
					//This function will print out if the 5 integer 'target' is found
					newCustomerIDMenu();
					target = idValidation(in);
					findCustomer(store, target, counter);
					break;

				case 5:
					//Taking input for a low and a high variable
					//Putting these numbers through a validation loop where low cannot be lower than 0 and low cannot be higher
					//than high. Once out of the validation loop, the low, high, store object, and counter are passed to a
					//function that print out if sale data is between low and high
					rangeMenu();
					low = in.nextInt();
					high = in.nextInt();

					while(rangeValidation(low, high) != true)
					{
						rangeMenu();
						low = in.nextInt();
						high = in.nextInt();

						rangeValidation(low, high);
					}
					
						findCustomersInRange(low, high, store, counter);
					
					break;
			}
			
			//Showing menu and prompting for choice that is put through a validation loop

			mainMenuChoice = mainMenuChoiceValidation(in);

		}

		//Exit message to show program has finished, referencing hit show 'Cowboy Bebop'
		newLine(1);
		System.out.println("See you, Space Cowboy");
		
		//Closing scanner object to save memory
		in.close();
	}

	/*
	 * List of menu's showing different options and prompting for different information
	 */
	public static void mainMenu()
	{
		newLine(1);
		System.out.println("           Menu");
		System.out.println("1: Add multiple new customers");
		System.out.println("2: Add single new customer");
		System.out.println("3: Display all customers");
		System.out.println("4: Retrieve specific customer's data");
		System.out.println("5: Retrieve customers with orders based on range");
		System.out.println("9: Exit program");
		newLine(1);
		System.out.print("Enter your selection: ");
	}

	public static void newCustomerNameMenu()
	{
		newLine(1);
		System.out.println("Please enter your customer's name.");
	}

	public static void newCustomerIDMenu()
	{
		newLine(1);
		System.out.println("Please enter your customer's ID.");
	}

	public static void newCustomerSaleMenu()
	{
		newLine(1);
		System.out.println("Please enter your customer's total sale.");
	}

	public static void rangeMenu()
	{
		newLine(1);
		System.out.println("Please enter the low and high value seperated by a space.");
	}

	/*
	 * Taking input, checking if these are options given within the main menu
	 * If it is not, will be stuck in a loop until you behave
	 */
	public static int mainMenuChoiceValidation(Scanner input)
	{
		int mainMenuChoice = 0;

		mainMenu();

		mainMenuChoice = input.nextInt();

		while(mainMenuChoice != 1 && mainMenuChoice != 2 && mainMenuChoice != 3 && mainMenuChoice != 4 && mainMenuChoice != 5 && mainMenuChoice != 9)
		{
			newLine(1);
			System.out.println("That is not a option on the menu");

			mainMenu();

			mainMenuChoice = input.nextInt();

		}

		return mainMenuChoice;	
	}

	/*
	 * Taking input, ensuring the number of users given is above 0 and below 100.
	 * There cannot be 0 or imaginary users, as this would throw an error
	 * There cannot be more than 100 users as the array only ranges from 0 - 99
	 */
	public static int numCustomersValidation(Scanner input)
	{
		int numCustomers = 0;

		newLine(1);
		System.out.println("How many customers would you like to enter? ");
		
		numCustomers = input.nextInt();

		while(numCustomers >= 100 || numCustomers <= 0)
		{
			if(numCustomers >= 100)
			{
				newLine(1);
				System.out.println("I think 100 customers are enough,\nif you want more wait until arraylists");
			}
			else if(numCustomers == 0)
			{
				newLine(1);
				System.out.println("There cannot be no customers, we need buiness.");
			}
			else
			{
				newLine(1);
				System.out.println("No imaginary customers");
			}
			
			newLine(1);
			System.out.println("How many customers would you like to enter? ");

			numCustomers = input.nextInt();
		}
		return numCustomers;
	}

	/*
	 * Taking input ensuring the user ID is a length of 5 numbers
	 */
	public static int idValidation(Scanner in)
	{
		int id = 0;

		id = in.nextInt();

		while ((id <= 9999) || (id >= 100000))
		{
			newLine(1);
			System.out.println("Sorry, customer ID's must be a 5 digit number");

			newCustomerIDMenu();
			
			id = in.nextInt();
		}

		return id;
	}

	/*
	 * Taking the low and high variables, then checking to ensure low is not greater than high, and that low is not lower than 0
	 * If it is, then you are put into time out until and prompted for low and high inputs again and again.
	 */
	public static boolean rangeValidation(int low, int high)
	{
		boolean rangeBoolean = true;
	
		if (!(low < high) || !(low > 0))
		{
			if(low > high)
			{
				newLine(1);
				System.out.println("Your low value cannot be more than your high value");
			}
			else
			{
				newLine(1);
				System.out.println("Your low value cannot be less than 1");
			}
			rangeBoolean = false;
		}
		
		return rangeBoolean;
	}

	/*
	 * Prompting user to enter a name, ID which is passed into the validation function, and sales data
	 * This is then passed as parameters to create an object named X which holds this data
	 * This is then passed back to the main function and stored in an array of objects
	 */
	public static Customer newCustomer(Scanner in)
	{
		int id = 0;
		double sale = 0;
		String name = "";

		newCustomerNameMenu();

		in.nextLine();
		name = in.nextLine();

		newCustomerIDMenu();

		id = idValidation(in);

		newCustomerSaleMenu();

		sale = in.nextDouble();

		Customer x = new Customer(name, id, sale);

		return x;
	}

	/*
	 * Function to traverse through the array, print out every value that is not the value null.
	 */
	public static void printCustomers(Customer store[])
	{
		for(int i = 0; i < store.length; i++)
		{
			if(store[i] != null)
			{
				newLine(1);
				System.out.printf("Name: %s ID: %d Sales total: $%.2f\n", store[i].getCustomerName(), store[i].getCustomerID(), store[i].getCustomerSale());
			}
		}
	}

	/*
	 * Function to traverse through the array, print out every customer that matches the "target" which is a user inputed ID
	 * If there is not a match, then output a statement to the console
	 */
	public static void findCustomer(Customer store[], int target, int counter)
	{
		boolean found = false;
		
		for (int i = 0; i < counter; i++)
		{
			if (store[i].getCustomerID() == target)
			{
				newLine(1);
				System.out.printf("Name: %s ID: %d Sales total: $%.2f\n", store[i].getCustomerName(), store[i].getCustomerID(), store[i].getCustomerSale());
				found = true;
			}
		}
		
		if(found == false)
		{
			newLine(1);
			System.out.println("There is not a customer that has the ID of: " + target);
		}
		
	}

	/*
	 * Function to traverse through the array, print out every customer whose sales data fits within the low and high range
	 * If there are no customers with sales data within the range, then output a statement to the console
	 */
	public static void findCustomersInRange(int low, int  high, Customer store[], int counter)
	{
		boolean found = false;
		
		for (int i = 0; i < counter; i++)
		{
			if (store[i].getCustomerSale() > low && store[i].getCustomerSale() < high)
			{
				newLine(1);
				System.out.printf("Name: %s ID: %d Sales total: $%.2f\n", store[i].getCustomerName(), store[i].getCustomerID(), store[i].getCustomerSale());
				found = true;
			}
		}
		
		if (found == false)
		{
			newLine(1);
			System.out.println("There are no customers that have sales between " + low + " and " + high);
		}
	}

	/*
	 * Recursive new line function, takes in an integer and prints out that many new lines
	 */
	public static void newLine(int n)
	{
		
		if(n != 0)
		{
			System.out.print("\n");
			newLine(n-1);
		}
	}

}

